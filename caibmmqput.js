//Imports
var mq = require('ibmmq');
var MQC = mq.MQC; //contains Message Queue Constant definitions

//Queue Manager Info
var qMgr = "QM1";
var qName = "DEV.QUEUE.1"

//Message Variable
var msg = null; //Initialized as null

function formatErr(err){
    return "MQ call failed (" + err.message + ").";
}

//Function to convert Hex MessageID to HexString
function toHexString(byteArray) {
    return byteArray.reduce((output, elem) =>
      (output + ('0' + elem.toString(16)).slice(-2)),
      '');
  }
  
  // Default Test Message - change with custom function
  function putMessage(hObj) {
  
    if(msg = null){
      msg = "Hello from Node at " + new Date();
      console.log("Default message:" + msg);
    }
  
    var mqmd = new mq.MQMD(); // Default MQ Message Descriptor.
    var pmo = new mq.MQPMO(); // Default MQ Put message options.
  
    //Put behaviour
    pmo.Options = MQC.MQPMO_NO_SYNCPOINT |
                  MQC.MQPMO_NEW_MSG_ID |
                  MQC.MQPMO_NEW_CORREL_ID;
  
    mq.Put(hObj,mqmd,pmo,msg,function(err) {
      if (err) {
        console.log(formatErr(err));
      } else {
        console.log("MsgId: " + toHexString(mqmd.MsgId));
        console.log("MQ PUT successful");
      }
    });
  }

//Closing Queues and connections
function cleanup(hConn,hObj) {
    mq.Close(hObj, 0, function(err) {
      if (err) {
        console.log(formatErr(err));
      } else {
        console.log("MQCLOSE successful");
      }
      mq.Disc(hConn, function(err) {
        if (err) {
          console.log(formatErr(err));
        } else {
          console.log("MQDISC successful");
        }
      });
    });
  }


//Put CODE Starts here
console.log("IBM MQ PUT start");

// Get command line parameters
var myArgs = process.argv.slice(2); // Remove extra parameters node, script path
if (myArgs[0]) {
  msg = myArgs[0];
  console.log("Custom Message: " + msg);
}
if (myArgs[1]) {
  qName = myArgs[1];
}
if (myArgs[2]) {
  qMgr = myArgs[2];
}

var cno = new mq.MQCNO();
cno.Options = MQC.MQCNO_NONE; // Connect as client

mq.Connx(qMgr, cno, function(err,hConn) {
   if (err) {
     console.log(formatErr(err));
   } else {
     console.log("MQCONN to %s successful ", qMgr);

     // Define what we want to open, and how we want to open it.
     var od = new mq.MQOD();
     od.ObjectName = qName;
     od.ObjectType = MQC.MQOT_Q;
     var openOptions = MQC.MQOO_OUTPUT;
     mq.Open(hConn,od,openOptions,function(err,hObj) {
       if (err) {
         console.log(formatErr(err));
       } else {
         console.log("MQOPEN of %s successful",qName);
         putMessage(hObj);
       }
       cleanup(hConn,hObj);
     });
   }
});