# Credence Analytics
## IBM MQ PUT and GET implementation using Node.js
### Installation
**Pre-Requisite:** IBM MQ Installed and Message Queue Configured

1. Install IBM MQ Developer Edition (detailed guide can be found on the official site [IBM MQ Downloads for developers](https://developer.ibm.com/components/ibm-mq/articles/mq-downloads/ "IBM MQ for Developers")).  *Tip: You can choose to install the IBM MQ Explorer GUI if needed.*
2. Setup IBM MQ (*Tip: root/administrator user privelages will be required to run commands*).
    1. ``` crtmqm QM1 ``` - Create Queue Manager with name "QM1"
    2. ``` strmqm QM1 ``` - Start Queue Manager with name "QM1"
    3. ``` runmqsc QM1 ``` - Execute MQ Service commands on Queue Manager with name "QM1"
    4. ``` define qlocal DEV.QUEUE.1 ``` - Create Local Queue Queue with name "DEV.QUEUE.1"
    5. ``` end ``` - End MQSC

**Running the Project**

1. Download and extract project to desired folder *OR* Clone Repository
2. ```npm install``` - Install npm modules and dependencies 
2. ``` node caibmmqput.js <message> <qName> <qManagerName> ``` - Put message command: Where *messaage* is the custom message, *qName* and *qManagerName* are names of queue and queue manager respectively. (If no arguments are assignend a default message will be sent to queue "DEV.QUEUE.1" in Queue Manager "QM1").
3. ``` node caibmmqget.js <qName> <qManagerName> ``` - Get all messages:  Where *qName* and *qManagerName* are names of queue and queue manager respectively.
